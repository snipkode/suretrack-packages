## SureTrack App v1.0

- Pengembangan Packages Socket IO dan Express
- Support POSTGRESQL dan MSSQL

- Setup Database ENV on env.example to .env and Change the value of them
```shell
USER_POSTGRESQL=
USER_PASSWORD_POSTGRESQL=
SERVER_POSTGRESQL=
DATABASE_NAME_POSTGRESQL=

USER_MSSQL=
SERVER_MSSQL=
DATABASE_NAME_MSSQL=
USER_PASSWORD_MSSQL=

```
- Connect database function

```javascript
const { db } = require('suretrack-backend');

(async () => {
    await db.connectToMssql();
    await db.connectToPg();
})();

```

- Quick Start Create Server

```javascript
const { app, json, cors } = require('suretrack-backend');

app.use(json());
app.use(cors());

app.use('/', (req, res) => {
    return res.sendStatus(200).send({
        message: "Hello World!"
    });
});

```

- Run Socket Event

```javascript

const { io } = require('suretrack-backend');

io.on('connection', (socket) => {

    socket.on('login', () => {
        console.log("User connected ", socket.id);
    });

    socket.on('disconnect', () => {
        console.log("User disconnected ", socket.id);
    });
});

```

- Jsonwebtoken Helper

```javascript
const { createToken, verifyToken, hashMD5, getUserBySocketId, getUserActiveBySocketId, getCurrentUser, softRemoveUserBySocketId, readSessions, saveSessions, removeUserBySocketId, removeUserByUserId, updateUserByUserId, addOrUpdateUser } = require('suretrack-backendf');
```

## RUNNING SERVER

- Create index.js

```shell
touch index.js

```

- Write script as follow :
```javascript
const { app } = require('suretrack-backend');

app.use('/', (req, res) => {
    return res.sendStatus(200).send({
        message: "Hello World!"
    });
});

```

- Install Package dependensi
```shell

npm i suretrack-backend

```

- Run server development

```shell

npm run start
```

- Link Repo https://gitlab.com/snipkode/suretrack-packages.git 