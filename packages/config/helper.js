const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

function formatDateToYYYYMMDD() {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0'); // Menggunakan padStart untuk memastikan dua digit
    const day = String(today.getDate()).padStart(2, '0'); // Menggunakan padStart untuk memastikan dua digit

    return `${year}-${month}-${day}`;
}

/**
 * Fungsi untuk menghasilkan hash MD5 dari string yang diberikan.
 * @param {string} data - String yang akan di-hash.
 * @returns {string} - Hash MD5 dari string yang diberikan.
 */
function hashMD5(data) {
    return crypto.createHash('md5').update(data).digest('hex');
}

if(!process.env.PRODUCTION){
    require('dotenv').config();
}

// Secret key untuk menandatangani token
const secretKey = process.env.JWT_SECRET || 'u6{gHNaa';

/**
 * Fungsi untuk membuat JWT.
 * @param {Object} payload - Data yang akan disimpan dalam token.
 * @returns {string} - Token JWT.
 */
function createToken(payload, options = { expiresIn: '5m' }) {
    // Membuat token dengan payload dan secret key
    const token = jwt.sign(payload, secretKey, options);
    return token;
}


/**
 * Fungsi untuk memverifikasi apakah token JWT valid atau tidak.
 * @param {string} token - Token JWT yang akan diverifikasi.
 * @returns {boolean} - True jika token valid, false jika tidak valid.
 */
function verifyToken(token) {
    try {
        // Memverifikasi token dengan secret key
        const userData = jwt.verify(token, secretKey);

        // Jika tidak ada error, token valid
        return userData;
    } catch (err) {
        // Jika ada error, token tidak valid
        console.error('Invalid token:', err);
        return false;
    }
}


/**
 * Fungsi untuk membaca data sesi dari file JSON.
 * @returns {Object} - Data sesi yang terbaca.
 */
function readSessions() {
    try {
        const data = fs.readFileSync(sessionFilePath, 'utf-8');
        return JSON.parse(data);
    } catch (error) {
        console.error('Error reading sessions:', error);
        return {};
    }
}

const sessionDir = path.join('./audit/sessions');
const sessionFilePath = path.join(sessionDir, `sessions-${formatDateToYYYYMMDD()}.json`);

// Cek apakah direktori ada, jika tidak buat direktori
if (!fs.existsSync(sessionDir)) {
    fs.mkdirSync(sessionDir, { recursive: true });
    // Buat file di dalam direktori yang sudah ada atau baru dibuat
    fs.writeFileSync(sessionFilePath, JSON.stringify({}), 'utf8');
    console.log(`Directory created: ${sessionDir}`);
}

/**
 * Fungsi untuk menyimpan data sesi ke file JSON.
 * @param {Object} sessions - Data sesi yang akan disimpan.
 */
function saveSessions(sessions, path = sessionFilePath) {
    try {
        fs.writeFileSync(path, JSON.stringify(sessions, null, 3), 'utf-8');
    } catch (error) {
        console.error('Error saving sessions:', error);
    }
}

/**
 * Fungsi untuk menambah atau mengupdate sesi pengguna.
 * @param {string} userId - ID pengguna.
 * @param {Object} sessionData - Data sesi pengguna yang akan disimpan atau diupdate.
 */

function addOrUpdateUser(userId, sessionData) {
    const sessions = readSessions();
    const existingSession = sessions[userId];
    if (existingSession) {
        // Jika sesi sudah ada, update sesi
        sessions[userId] = { ...existingSession, ...sessionData };
        console.log(`Session updated for user ${userId}`);
    } else {
        // Jika sesi belum ada, tambahkan sesi baru (Anonymous User)
        sessions[userId] = sessionData;
        console.log(`New session added for user ${userId}`);
    }
    saveSessions(sessions);
}

/**
 * Fungsi untuk mengupdate sesi pengguna.
 * @param {string} userId - ID pengguna.
 * @param {Object} sessionData - Data sesi pengguna yang akan disimpan atau diupdate.
 */

function updateUserByUserId(userId, sessionData) {
    const sessions = readSessions();
    const existingSession = sessions[userId];
    if (existingSession) {
        /* Jika sesi sudah ada, update sesi */
        sessions[userId] = { ...existingSession, ...sessionData };
        console.log(`User session dengan id ${userId} telah diperbaharui`);
    }
    saveSessions(sessions);
}

/**
 * Fungsi untuk mendapatkan data pengguna saat ini berdasarkan ID pengguna.
 * @param {string} userId - ID pengguna.
 */
function getCurrentUser(userId) {
    const sessions = readSessions();
    const existingSession = sessions[userId];
    return existingSession;
}

/**
 * Fungsi untuk menghapus sesi pengguna berdasarkan ID pengguna.
 * @param {string} userId - ID pengguna.
 */

function removeUserByUserId(userId) {
    const sessions = readSessions();
    if (sessions[userId]) {
        delete sessions[userId];
        saveSessions(sessions);
        console.log(`Session deleted for user ${userId}`);
    } else {
        console.log(`No session found for user ${userId}`);
    }
}

function removeUserBySocketId(socketId) {
    const sessions = readSessions();
    let deleted = false;
    Object.keys(sessions).forEach(key => {
        if (sessions[key].socket_id === socketId) {
            delete sessions[key];
            deleted = true;
        }
    });

    saveSessions(sessions);
}

function softRemoveUserBySocketId(socketId) {
    const sessions = readSessions();
    let updated = false;

    for (const userId in sessions) {
        if (sessions[userId].socket_id === socketId) {
            sessions[userId].socket_id = null;
            updated = true;
            break;
        }
    }

    saveSessions(sessions);
}

function getUserActiveBySocketId() {
    const sessions = readSessions();
    let count = 0;

    for (const userId in sessions) {
        if (sessions[userId].socket_id !== null) {
            count++;
        }
    }

    return count;
}


function getUserBySocketId(socketId) {
    const sessions = readSessions();

    for (const userId in sessions) {
        if (sessions[userId].socket_id === socketId) {
            return userId;
        }
    }

    return null;
}


module.exports = { createToken, verifyToken, hashMD5, getUserBySocketId, getUserActiveBySocketId, getCurrentUser, softRemoveUserBySocketId, readSessions, saveSessions, removeUserBySocketId, removeUserByUserId, updateUserByUserId, addOrUpdateUser };
