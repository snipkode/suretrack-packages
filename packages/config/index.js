const { Pool } = require('pg');
const mssql = require('mssql');
const { createLogger, format, transports } = require('winston');

function formatDateToYYYYMMDD() {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0'); // Menggunakan padStart untuk memastikan dua digit
    const day = String(today.getDate()).padStart(2, '0'); // Menggunakan padStart untuk memastikan dua digit

    return `${year}-${month}-${day}`;
}

class Config {
    constructor({ mssqlConfig, postgresqlConfig, debug }) {
        this.pgPool = new Pool(postgresqlConfig);
        this.mssqlPool = new mssql.ConnectionPool(mssqlConfig);
        this.debug = debug;

        this.logger = createLogger({
            level: 'info',
            format: format.combine(
                format.timestamp(),
                format.printf(info => `${info.timestamp} [${info.level.toUpperCase()}]: ${info.message}`)
            ),
            transports: [
                new transports.Console(),
                new transports.File({ filename: `./audit/logs/access-${formatDateToYYYYMMDD()}.log` })
            ]
        });
    }

    async connectToPg() {

        try {
            await this.pgPool.connect();
            this.logger.info(`PostgreSQL connected`);
        } catch (error) {
            this.logger.error(`PostgreSQL`, error);
        }
    }

    async connectToMssql() {
        try {
            await this.mssqlPool.connect();
            this.logger.info(`MSSQL connected`);

        } catch (error) {
            this.logger.error(`MSSQL`, error);
        }

    }


    async executePg(query, params) {
        const client = await this.pgPool.connect();
        try {
            const res = await client.query(query, params);
            this.logger.info(`Executed PostgreSQL query: ${query}`);
            return res;
        } catch (err) {
            this.logger.error(`PostgreSQL query failed: ${query}`, err);
        } finally {
            client.release();
        }
    }

    async startPgTransaction() {
        try {
            const client = await this.pgPool.connect();
            await client.query('BEGIN');
            this.logger.info(`Begin Transaction POSTGRES`);
            return client;
        } catch (err) {
            this.logger.error('Error starting PostgreSQL transaction:', err);
            throw err;
        }
    }

    async commitPgTransaction(client) {
        try {
            await client.query('COMMIT');
            client.release();
            this.logger.info('PostgreSQL transaction committed successfully');

        } catch (err) {
            this.logger.error('Error committing PostgreSQL transaction:', err);
            throw err;
        }

    }

    async rollbackPgTransaction(client) {
        try {
            await client.query('ROLLBACK');
            client.release();
            this.logger.info('PostgreSQL transaction rolled back');

        } catch (err) {
            this.logger.error('Error rolling back PostgreSQL transaction:', err);
            throw err;
        }
    }

    async executeMssql(query, params) {
        try {
            const request = new mssql.Request(this.mssqlPool);
            if (params) {
                params.forEach((param, index) => {
                    request.input(`param${index}`, param);
                });
            }

            this.logger.info(`Executed MSSQL query: ${query}`);
            return request.query(query);

        } catch (err) {
            this.logger.error(`MSSQL query failed: ${query}`, err);
            throw err;
        }
    }

    async startMssqlTransaction() {
        try {
            const transaction = new mssql.Transaction(this.mssqlPool);
            await transaction.begin();
            this.logger.info(`Begin Transaction MSSQL`);
            return transaction;
        } catch (err) {
            this.logger.error('Error starting MSSQL transaction:', err);
            throw err;
        }
    }

    async commitMssqlTransaction(transaction) {
        try {
            await transaction.commit();
            this.logger.info('MSSQL transaction committed successfully');
        } catch (err) {
            this.logger.error('Error committing MSSQL transaction:', err);
            throw err;
        }
    }

    async rollbackMssqlTransaction(transaction) {
        try {
            await transaction.rollback();
            this.logger.info('MSSQL transaction rolled back');
        } catch (err) {
            this.logger.error('Error rolling back MSSQL transaction:', err);
            throw err;
        }

    }

    getLogger() {
        return this.logger;
    }
}

const { mssqlConfig, pgConfig } = require('./connection');

const db = new Config({ mssqlConfig: mssqlConfig, postgresqlConfig: pgConfig, debug: false });
const Joi = require('joi');
const logger = db.getLogger();

module.exports = { db, Joi, logger };
