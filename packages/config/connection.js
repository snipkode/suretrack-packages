if (!process.env.PRODUCTION) {
    require('dotenv').config();
}

const mssqlConfig = {
    user: process.env.USER_MSSQL,
    password: process.env.USER_PASSWORD_MSSQL,
    server: process.env.SERVER_MSSQL,
    database: process.env.DATABASE_NAME_MSSQL,
    options: {
        encrypt: false,
        enableArithAbort: true,
        trustServerCertificate: true,
        connectionTimeout: 5000
    }
};

const pgConfig = {
    user: process.env.USER_POSTGRESQL,
    host: process.env.SERVER_POSTGRESQL,
    database: process.env.DATABASE_NAME_POSTGRESQL,
    password: process.env.USER_PASSWORD_POSTGRESQL,
    port: 5432,
    ssl: false,
    reconnect: true,
    connectionTimeoutMillis: 5000
};

module.exports = { pgConfig, mssqlConfig };