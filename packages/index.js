const express       = require('express');
const http          = require('http');
const socketIO      = require('socket.io');
const cors          = require('cors');

const app           = express();
const router        = express.Router;
const json          = express.json;
const server        = http.createServer(app);
const io            = socketIO(server);

const { createToken, verifyToken, hashMD5, getUserBySocketId, getUserActiveBySocketId, getCurrentUser, softRemoveUserBySocketId, readSessions, saveSessions, removeUserBySocketId, removeUserByUserId, updateUserByUserId, addOrUpdateUser } = require('./config/helper');

if(!process.env.PRODUCTION){
    require('dotenv').config();
}
const {db, Joi, logger} = require('./config');
const SERVER_PORT       = process.env.SERVER_PORT || 5000;

server.listen(SERVER_PORT, () => {
    console.log(`\n ===== Server is running on port http://localhost:${SERVER_PORT} ===== \n`);
});

module.exports = { app, router, server, io, json, cors, db, Joi, logger, createToken, verifyToken, hashMD5, getUserBySocketId, getUserActiveBySocketId, getCurrentUser, softRemoveUserBySocketId, readSessions, saveSessions, removeUserBySocketId, removeUserByUserId, updateUserByUserId, addOrUpdateUser };