const { io, app, json, cors, saveSessions } = require('suretrack-backend');

app.use(json());
app.use(cors());

app.use('/', (req, res) => {
    return res.sendStatus(200).send({
        message: "Hello World!"
    });
});

saveSessions({ data: 1});

io.on('connection', (socket) => {
    
    socket.on('login', () => {
        console.log("User connected ", socket.id);
    });

    socket.on('disconnect', () => {
        console.log("User disconnected ", socket.id);
    });
});
